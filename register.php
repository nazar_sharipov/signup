<?php

require_once('forms/RegistrationForm.php');
require_once('components/DB.php');
require_once('components/Password.php');

$params = include('configs/db_params.php');

$db_host = $params['db_host']; 
$db_user = $params['db_user'];
$db_password = $params['db_password'];
$db_name = $params['db_name'];

$db = new DB($db_host, $db_user, $db_password, $db_name);
$form = new RegistrationForm($_POST);
$nameErr = $emailErr = $genderErr = $passErr = $telErr = $confPassErr = "";
$msg = '';

if ( $_POST ) { 
    if ( !$form->validate() ) {
        $nameErr = $form->nameValidate() ? null : "Name is too short";
        $emailErr = $form->emailValidate() ? null : "Please enter a valid email address";
        $telErr = $form->phoneValidate() ? null : "Please enter a valid phone number";
        $passErr = $form->passValidate() ? null : "Password is too short";
        $confPassErr = $form->confirmPassValidate() ? null : "Confirm Passwords is too short";
        $genderErr = empty($_POST['gender']) ? "Gender is required" : null;
        $msg = $form->passwordsMatch() ? "All fields are required" : "Passwords don't match";
    } else {
        $email = $db->escape($form->email); 
        $username = $db->escape($form->username);
        $phone = $db->escape($form->phone);
        $gender = $db->escape($form->gender);
        $password = new Password($db->escape($form->password));      

        $res = $db->query("SELECT * FROM user WHERE username = '{$username}'");
        if ( $res ) {
            $msg = 'Such user already exists!';
        } else {
            $db->query("INSERT INTO user (email, username, password, phone, gender) VALUES ('{$email}','{$username}','{$password}','{$phone}','{$gender}')");
            header('location: index.php?msg=You have been registered');
        }
    }
}
 

$title = 'Sign Up';
require('layout/header.php'); 
?>
<html>
<body>
    <div class="welcome"><?=$msg; ?></div>
    <h2 class="greeting">Please Sign Up</h2>
    <form class="reg_form" method="post">
        <p><span class="error">* required field.</span></p>
        <div class="data-input">
            <!-- <label for="username" >Name</label><br> -->
            <input type="text" name="username" placeholder="User Name" 
                        maxlength="40" value="<?=$form->username; ?>"/>
            <span class="error">* <?=$nameErr;?></span></div>
        <div class="data-input">
            <!-- <label for="email" >Email</label><br> -->
            <input type="email" name="email" placeholder="example@mail.com" 
                    maxlength="32" value="<?=$form->email; ?>"/>
            <span class="error">* <?=$emailErr;?></span> 
        </div>
        <div class="data-input">
            <!-- <label for="password" >Password</label><br> -->
            <input type="password" name="password" placeholder="Password" 
                    value="<?=$form->password; ?>" />
            <span class="error">* <?=$passErr;?></span> 
        </div>
        <div class="data-input">
            <!-- <label for="passwordConfirm" >Confirm Password</label><br> -->
            <input type="password" name="passwordConfirm" placeholder="Confirm Password" />
            <span class="error">* <?=$confPassErr;?></span> 
        </div>
        <div class="data-input">
        <!-- <label for="phone" >Phone Number</label><br> -->
        <input type="text" name="phone" placeholder="0xx xxx xxxx"
                value="<?=$form->phone; ?>" /> 
        <span class="error">* <?=$telErr;?></span>
        </div>
        <div class="data-input wrapper-gender">
            <label for="gender" ><b class="err-flag left-flag">*</b> Gender: Male</label>
            <input class="gender" type="radio" name="gender" value="male" <?php if ($form->gender == 'male') echo 'checked'; ?> >
            <label for="gender" >Female</label>
            <input class="gender" type="radio" name="gender" value="female" <?php if ($form->gender == 'female') echo 'checked'; ?> >
            <span class="error error-gender"><b class="err-flag right-flag">* </b><?=$genderErr;?></span>
        </div>
        <button class="submit" type="submit" >Register</button>
    </form>
    <h5 class="wrapper"><a class="footer-link" href="index.php">Back to homepage</a></h5>

<?php require('layout/footer.php'); ?>