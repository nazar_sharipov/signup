<?php
session_start();
require_once 'components/Session.php';

Session::destroy();

header('Location: index.php');