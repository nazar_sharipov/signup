<?php
session_start();

require_once 'forms/LoginForm.php';
require_once 'components/DB.php';
require_once 'components/Password.php';
require_once 'components/Session.php';

$params = include('configs/db_params.php');

$db_host = $params['db_host'];
$db_user = $params['db_user']; 
$db_password = $params['db_password'];
$db_name = $params['db_name'];



$msg = '';

$db = new DB($db_host, $db_user, $db_password, $db_name);
$form = new LoginForm($_POST);

if ( $_POST ) {
    if ( $form->validate() ) {
        $username = $db->escape($form->username);
        $password = new Password($db->escape($form->password));

        $res = $db->query("SELECT * FROM user WHERE username = '{$username}' AND password = '{$password}' LIMIT 1");
        if ( !$res ) {
            $msg = 'The username or password you entered is incorrect.';
        } else {
            $user = $res[0]['username'];
            Session::set('user', $user);
            header('location: index.php?msg=You have been logged in');
        }

    } else {
        $msg = 'Please fill in fields';
    }
}

//define page title
$title = 'Sign In';

//include header template
require('layout/header.php'); ?>
<html>
<body>
    <?=$msg;?>  
    <br/>
    <h2 class="greeting">Please Login</h2>
    <form class="reg_form" method="post">
        <div class="data-input">
            <input type="text" name="username" placeholder="User Name" value="<?=$form->username; ?>"/> 
        </div>
        <div class="data-input">
            <input type="password" name="password" placeholder="Password" /> 
        </div>
        <button class="submit" type="submit" >Login</button>
    </form>
    <h5 class="wrapper" ><a class="footer-link" href="index.php">Back to homepage</a></h5>

<?php require('layout/footer.php'); ?>