<?php

class RegistrationForm {
    private $email;
    private $username;
    private $password;
    private $passwordConfirm;
    private $phone;
    private $gender;


    /**
     * @param array $data
     */
    function __construct(Array $data) {
        $this->email = trim(isset($data['email']) ? $data['email'] : null);
        $this->username = trim(isset($data['username']) ? $data['username'] : null);
        $this->password = trim(isset($data['password']) ? $data['password'] : null);
        $this->passwordConfirm = trim(isset($data['passwordConfirm']) ? $data['passwordConfirm'] : null);
        $this->phone = trim(isset($data['phone']) ? $data['phone'] : null);
        $this->gender = $data['gender'];
    }

    public function validate() {
        return !empty($this->email) && !empty($this->username) && !empty($this->password) 
            && !empty($this->passwordConfirm) && $this->passwordsMatch() 
            && !empty($this->gender) && !empty($this->phone);
    }

    /**
     * @return bool
     */
    public function nameValidate() {
        return strlen($this->username) > 3;
    }

    /**
     * @return bool
     */
    public function passValidate() {
        return strlen($this->password) > 5;
    }

    /**
     * @return bool
     */
    public function emailValidate() {
        return filter_var($this->email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @return bool
     */
    public function phoneValidate() {
        return  preg_match('/^0[0-9]{2} [0-9]{3} [0-9]{4}$/', $this->phone);
    }

    /**
     * @return bool
     */
    public function confirmPassValidate() {
        return strlen($this->passwordConfirm) > 5;
    }

    /**
     * @return bool
     */
    public function passwordsMatch() {
        return $this->password == $this->passwordConfirm;
    }



    public function __get($name) {
        if ( property_exists($this, $name) ) {
            return $this->$name;
        }
        throw Exception("Attribute error: attribute $name not found");
    }

    public function __set($name, $value) {
        if ( property_exists($this, $name) ) {
            $this->$name = $this->$value;
        }
        throw Exception("Attribute error: attribute $name not found");
    }
} 

