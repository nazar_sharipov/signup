<?php


class LoginForm {
    private $username;
    private $password;

    /**
     * @param array $data
     */
    public function __construct(Array $data) {
        $this->username = trim(isset($data['username']) ? $data['username'] : null);
        $this->password = trim(isset($data['password']) ? $data['password'] : null );
    }

    /**
     * @return bool
     */
    public function validate() {
        return !empty($this->username) && !empty($this->password);
    }

    public function __get($name) {
            if ( property_exists($this, $name) ) {
                return $this->$name;
            }
            throw Exception("Attribute error: attribute $name not found");
        }

    public function __set($name, $value) {
        if ( property_exists($this, $name) ) {
            $this->$name = $this->$value;
        }
        throw Exception("Attribute error: attribute $name not found");
    }
}
