<?php
session_start();
require_once 'components/Session.php';

$title = 'Homepage';

//include header template
require('layout/header.php');
?>
<html>
<body>
<div class="menu">
<?php if (!Session::has('user')) : ?>
    <a class="ref-link" href="register.php">Sign Up</a>
<?php endif; ?>

<?php if (Session::has('user')) : ?>
    <a class="ref-link" href="logout.php">Logout (<?=Session::get('user'); ?>)</a>
<?php else : ?>
    <a class="ref-link" href="login.php">Login</a>
<?php endif; ?>
</div>

<br/>
<div class="greeting"><?=isset($_GET['msg']) ? $_GET['msg'] : '';?></div>

<?php require('layout/footer.php'); ?>
